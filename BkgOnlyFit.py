from ROOT import *
from Hfitter import *
from math import *
import argparse
import sys

def AtlasLabelInternal( x, y, color = 1 , size = 0.032):            
    l = TLatex()
    l.SetNDC()
    l.SetTextSize( size )
    l.SetTextFont( 72 )
    l.SetTextColor( color )
    #  l.DrawLatex( x, y, "ATLAS" )
    l.DrawLatex( x, y, "ATLAS #font[42]{Internal}" )
    return

def SetConfig():
    parser = argparse.ArgumentParser(description="Set configuration")
    parser.add_argument("-i","--inputFile", required=True, help="Input Data rootfile.")
    parser.add_argument("-hist","--inputHist", required=True, help="Input dilepton mass hist.")
    parser.add_argument("-c","--channel", required=True, help="Choose ee or mm channel.")
    parser.add_argument("-l","--rangeLow", default=180, help="Set FixRange Low value.")
    parser.add_argument("-u","--rangeHigh", default=6000, help="Set FixRange High value.")
    return parser

def BkgModelBuild_ee(mreco):
    pE_ee = RooRealVar("pE_ee","",5.36419,0.5,15)
    p0_ee = RooRealVar("p0_ee","",-11.0799, -50,0)
    p1_ee = RooRealVar("p1_ee","",-4.16261, -20,0)
    p2_ee = RooRealVar("p2_ee","",-0.952427, -5,0)
    p3_ee = RooRealVar("p3_ee","",-0.0901591, -1,0)
    formula = TString()
    formula.Form("(2.4952/(pow(91.1876 - mreco,2)+pow(2.4952,2)))*pow(1 - mreco/13000, pE_ee)*pow(mreco/13000, p0_ee+p1_ee*log(mreco/13000)+p2_ee*log(mreco/13000)^2+p3_ee*log(mreco/13000)^3)")
    return pE_ee,p0_ee,p1_ee,p2_ee,p3_ee,formula

def BkgModelBuild_mm(mreco):
    pE_mm = RooRealVar("pE_mm","",1.88782,0.5,15)
    p0_mm = RooRealVar("p0_mm","",-11.7032, -50,0)
    p1_mm = RooRealVar("p1_mm","",-4.21916, -20,0)
    p2_mm = RooRealVar("p2_mm","",-0.931291, -5,0)
    p3_mm = RooRealVar("p3_mm","",-0.0869486, -1,0)
    formula = TString()
    formula.Form("(2.4952/(pow(91.1876 - mreco,2)+pow(2.4952,2)))*pow(1 - pow(mreco/13000, 1./3.), pE_mm)*pow(mreco/13000, p0_mm+p1_mm*log(mreco/13000)+p2_mm*log(mreco/13000)^2+p3_mm*log(mreco/13000)^3)")
    return pE_mm,p0_mm,p1_mm,p2_mm,p3_mm,formula

def InputData(args,mreco):
    InputFile = TFile.Open(str(args.inputFile))
    DataHist = InputFile.Get(str(args.inputHist))
    data = RooDataHist("dh", "dh", RooArgList(mreco), RooFit.Import(DataHist))
    return data

def DrawPlot(mreco, GenPdf, data, args):
    c = TCanvas("interprfuncs", "interprfuncs", 800, 900)
    pad1 = TPad("pad1","",0.,0.3,1.,1.)
    pad1.SetLogx()
    #pad1.SetLogy()
    pad1.SetBottomMargin(0.01)
    pad1.SetLeftMargin(0.11)
    pad1.SetRightMargin(0.02)
    pad1.Draw()
    pad1.cd()
    xframe = mreco.frame(RooFit.Title("Run3_NewVersion_Reco_Dilepton_MassHist_{}".format(args.channel)))
    data.plotOn(xframe,RooFit.LineColor(2))
    GenPdf.plotOn(xframe,RooFit.LineColor(4))
    xframe.GetXaxis().SetRangeUser(225,5000)
    xframe.GetYaxis().SetRangeUser(0,8.2)
    xframe.GetXaxis().SetMoreLogLabels()
    xframe.Draw()
    AtlasLabelInternal(0.65,0.85)
    Latex = TLatex()
    Latex.SetTextSize(0.036)
    Latex.SetTextFont(42)
    #Latex.DrawLatexNDC(0.25, 0.8, "#sqrt{{s}} = 13.6TeV, L = {Luminosity} pb^{{-1}}".format(Luminosity = round(Luminosity,2)))
    Latex.DrawLatexNDC(0.65, 0.8, "#sqrt{s} = 13.6 TeV, Run3")
    Latex.DrawLatexNDC(0.65, 0.75, "{} channel".format(args.channel))
    c.cd()
    pad2 = TPad("Pad_RatioHist2", "Pad_RatioHist2", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0)
    pad2.SetLeftMargin(0.11)
    pad2.SetRightMargin(0.02)
    pad2.SetBottomMargin(0.2)
    pad2.SetGridy(1)
    pad2.SetLogx()
    pad2.Draw()
    pad2.cd()
    BinNumber = 120
    xMasslow = 225
    xMassHigh = 5000
    logMassLow = log10(xMasslow)
    logMassHigh = log10(xMassHigh)
    binwidth = (logMassHigh-logMassLow)/BinNumber
    tbins = RooBinning(225,5000)
    for i in range(BinNumber+1):
        tbins.addBoundary(pow(10,logMassLow+i*binwidth))
    new_hist_1 = (GenPdf.generateBinned(RooArgSet(mreco),100000,kTRUE)).createHistogram("mreco",RooFit.Binning(tbins))
    new_hist_2 = data.createHistogram("mreco",RooFit.Binning(tbins))
    new_hist_1.Scale(new_hist_2.Integral()/new_hist_1.Integral())
    new_hist_1.Divide(new_hist_2)
    new_hist_1.Draw("E")
    new_hist_1.SetMarkerStyle(20)
    new_hist_1.SetMarkerSize(1)
    new_hist_1.SetLineColor(kRed)
    ax = new_hist_1.GetXaxis()
    ax.SetTitle( " Reco_DiLepton_Mass (GeV) " )
    ay = new_hist_1.GetYaxis()
    ay.SetTitle( "Template/Fit" )
    ay.SetTitleSize(0.07)
    ax.SetTitleOffset(1)
    ay.SetRangeUser(0.5,1.5)
    ax.SetRangeUser(225,5000)
    ay.SetTitleOffset(0.6)
    ax.SetTitleSize(0.070)
    ax.SetMoreLogLabels()
    ax.SetLabelSize(0.065)
    ay.SetLabelSize(0.065)
    ax.Draw("same")
    ay.Draw("same")
    c.SaveAs("simPdf.png")
    


if __name__ == '__main__':
    
    # Build the Hfitter/HGGfitter environment
    load_libraries()
    # Set the configuraion
    parser = SetConfig()
    args = parser.parse_args()
    mreco = RooRealVar("mreco","",130,6000) 
    GenPdf = RooGenericPdf()
    # Build Bkg Model
    if args.channel == 'ee':
        pE_ee,p0_ee,p1_ee,p2_ee,p3_ee,formula = BkgModelBuild_ee(mreco)
        GenPdf = RooGenericPdf("Gen_pdf", "Gen_pdf", str(formula), RooArgList(mreco,pE_ee,p0_ee,p1_ee,p2_ee,p3_ee))
    elif args.channel == 'mm':
        pE_mm,p0_mm,p1_mm,p2_mm,p3_mm,formula = BkgModelBuild_mm(mreco)
        GenPdf = RooGenericPdf("Gen_pdf", "Gen_pdf", str(formula), RooArgList(mreco,pE_mm,p0_mm,p1_mm,p2_mm,p3_mm))
    else:
        print("========= Don't input the proper channel ==========")
    # Acquire DataHist
    data = InputData(args, mreco)
    # Bkg-only fit
    mreco.setRange("fitRange", args.rangeLow, args.rangeHigh)
    GenPdf.fitTo(data,RooFit.Range("fitRange"))
    DrawPlot(mreco, GenPdf, data, args)


